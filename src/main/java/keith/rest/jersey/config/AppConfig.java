package keith.rest.jersey.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import keith.spring.config.DataConfig;

/**
 * 
 * @author kjayme
 *
 */
@Configuration
// @Import(value = { ServiceConfig.class })
@Import(value = { DataConfig.class })
public class AppConfig {
}
