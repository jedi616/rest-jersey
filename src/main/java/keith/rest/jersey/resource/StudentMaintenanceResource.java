package keith.rest.jersey.resource;

import java.net.URI;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import keith.domain.dto.StudentDto;
import keith.domain.dto.UIResponse;
import keith.domain.jpa.entity.StudentM;
import keith.spring.datajpa.mapper.StudentMapper;
import keith.spring.datajpa.repository.StudentRepository;

/**
 * 
 * No Catch no Throws.
 * 
 * @author Keith F. Jayme
 *
 */
/**
 * @author kjayme
 *
 */
@Component
@Path("maintenance/student")
public class StudentMaintenanceResource {

    private static final String NOT_FOUND_ERROR_MESSAGE = "Student Not Found";
    private static final String FOUND_SUCCESS_MESSAGE = "Student Found";
    private static final String DELETE_SUCCESS_MESSAGE = "Student is Deleted";
    private static final String CREATE_SUCCESS_MESSAGE = "Student is Created";
    private static final String UPDATE_SUCCESS_MESSAGE = "Student is Updated";

    @Autowired
    private StudentRepository studentRepo;

    @Autowired
    private StudentMapper studentMapper;

    @GET
    @Path("retrieveall")
    @Produces(MediaType.APPLICATION_JSON)
    public Response retrieveAll(@Context HttpHeaders httpHeaders) throws Exception {
        // Sample retrieving HttpHeaders.
        MultivaluedMap<String, String> multivaluedMap = httpHeaders.getRequestHeaders();
        Object[] objArray = multivaluedMap.entrySet().toArray();

        // 200 (OK) - the representation is sent in the response
        UIResponse<List<StudentDto>> response = new UIResponse<List<StudentDto>>();
        List<StudentM> studentMs = studentRepo.findAll();
        List<StudentDto> studentDtos = studentMapper.mapToDtoList(studentMs);
        response.setData(studentDtos);
        return Response.ok(response).build();
    }

    @GET
    @Path("retrieveone/{studentid}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response retrieveOne(@PathParam("studentid") long studentId) throws Exception {
        // 200 (OK) - the representation is sent in the response
        // 404 (not found) - the resource does not exits
        UIResponse<StudentDto> response = new UIResponse<StudentDto>();
        StudentM studentM = studentRepo.findOne(studentId);
        if (studentM != null) {
            StudentDto studentDto = studentMapper.mapToDto(studentM);
            response.success(studentDto, FOUND_SUCCESS_MESSAGE);
            return Response.ok(response).build();
        } else {
            response.error(NOT_FOUND_ERROR_MESSAGE);
            return Response.status(Status.NOT_FOUND).entity(response).build();
        }
    }

    @POST
    @Path("create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response create(StudentDto studentDto, @Context UriInfo uriInfo) throws Exception {
        // 201 (created) - if a new resource is created
        // 409 (conflict) - general conflict
        studentDto.setStudentId(0); // Ensures that repo.save(entity) will do an insert and not an update.
        StudentM studentM = studentMapper.mapToEntity(studentDto);
        studentM = studentRepo.save(studentM);
        StudentDto createdStudentDto = studentMapper.mapToDto(studentM);

        UriBuilder uriBuilder = uriInfo.getBaseUriBuilder();
        uriBuilder.path("maintenance/student/retrieveone/" + createdStudentDto.getStudentId());
        URI uri = uriBuilder.build();
        return Response.created(uri).build();
    }

    @PUT
    @Path("update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response update(StudentDto studentDto, @Context UriInfo uriInfo) throws Exception {
        // 200 (OK) - if an existing resource has been updated
        // 404 (not found) - the resource does not exits
        // 409 (conflict) - general conflict
        UIResponse<StudentDto> response = new UIResponse<StudentDto>();
        StudentM studentM = studentRepo.findOne(studentDto.getStudentId());
        if (studentM != null) {
            studentMapper.mapToEntity(studentM, studentDto);
            studentM = studentRepo.save(studentM);
            StudentDto updatedStudentDto = studentMapper.mapToDto(studentM);
            response.success(updatedStudentDto, UPDATE_SUCCESS_MESSAGE);
            return Response.ok(response).build();
        } else {
            response.error(NOT_FOUND_ERROR_MESSAGE);
            return Response.status(Status.NOT_FOUND).entity(response).build();
        }
    }

    @DELETE
    @Path("delete/{studentid}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("studentid") long studentId) throws Exception {
        // 200 (OK) - the resource has been deleted
        // 404 (not found) - the resource does not exits
        // 409 (conflict) - general conflict
        UIResponse<StudentDto> response = new UIResponse<StudentDto>();
        StudentM studentM = studentRepo.findOne(studentId);
        if (studentM != null) {
            StudentDto studentDto = studentMapper.mapToDto(studentM);
            studentRepo.delete(studentM);
            response.success(studentDto, DELETE_SUCCESS_MESSAGE);
            return Response.ok(response).build();
        } else {
            response.error(NOT_FOUND_ERROR_MESSAGE);
            return Response.status(Status.NOT_FOUND).entity(response).build();
        }
    }

    // @PUT
    // @Path("update-void")
    // @Consumes(MediaType.APPLICATION_JSON)
    // public void updateVoid(StudentDto studentDto, @Context UriInfo uriInfo)
    // throws Exception
    // {
    // update(studentDto, uriInfo);
    // }

    // @DELETE
    // @Path("delete-void/{studentid}")
    // public void deleteVoid(@PathParam("studentid") long studentId) throws
    // Exception
    // {
    // delete(studentId);
    // }
}
