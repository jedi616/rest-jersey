package keith.rest.jersey.resource;

import java.net.URI;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import keith.domain.dto.CourseDto;
import keith.domain.dto.SelectItem;
import keith.domain.dto.UIResponse;
import keith.domain.jpa.entity.CourseM;
import keith.spring.datajpa.mapper.CourseMapper;
import keith.spring.datajpa.repository.CourseRepository;

/**
 * 
 * @author Keith F. Jayme
 *
 */
@Component
@Path("maintenance/course")
public class CourseMaintenanceResource {

    private static final String NOT_FOUND_ERROR_MESSAGE = "Course Not Found";
    private static final String FOUND_SUCCESS_MESSAGE = "Student Found";
    private static final String USED_BY_STUDENT_ERROR_MESSAGE = "Course is being used by a Student";
    private static final String DELETE_SUCCESS_MESSAGE = "Course is Deleted";
    private static final String CREATE_SUCCESS_MESSAGE = "Course is Created";
    private static final String UPDATE_SUCCESS_MESSAGE = "Course is Updated";

    @Autowired
    private CourseRepository courseRepo;

    @Autowired
    private CourseMapper courseMapper;

    @GET
    @Path("retrieveall")
    @Produces(MediaType.APPLICATION_JSON)
    public Response retrieveAll() throws Exception {
        UIResponse<List<CourseDto>> response = new UIResponse<List<CourseDto>>();
        List<CourseM> courseMs = courseRepo.findAll();
        List<CourseDto> courseDtos = courseMapper.mapToDtoList(courseMs);
        response.setData(courseDtos);
        return Response.ok(response).build();
    }

    @GET
    @Path("retrieveall/selectitem")
    @Produces(MediaType.APPLICATION_JSON)
    public Response retrieveAllSelectItem() throws Exception {
        UIResponse<List<SelectItem>> response = new UIResponse<List<SelectItem>>();
        List<CourseM> courseMs = courseRepo.findAll();
        List<SelectItem> courseSelectItems = courseMapper.getSelectItems(courseMs);
        response.setData(courseSelectItems);
        return Response.ok(response).build();
    }

    @GET
    @Path("retrieveone/{courseid}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response retrieveOne(@PathParam("courseid") long courseId) throws Exception {
        UIResponse<CourseDto> response = new UIResponse<CourseDto>();
        CourseM courseM = courseRepo.findOne(courseId);
        if (courseM != null) {
            CourseDto courseDto = courseMapper.mapToDto(courseM);
            response.success(courseDto, FOUND_SUCCESS_MESSAGE);
            return Response.ok(response).build();
        } else {
            response.error(NOT_FOUND_ERROR_MESSAGE);
            return Response.status(Status.NOT_FOUND).entity(response).build();
        }
    }

    @POST
    @Path("create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response create(CourseDto courseDto, @Context UriInfo uriInfo) throws Exception {
        courseDto.setCourseId(0); // Ensures that repo.save(entity) will do an insert and not an update.
        CourseM courseM = courseMapper.mapToEntity(courseDto);
        courseM = courseRepo.save(courseM);
        CourseDto createdCourseDto = courseMapper.mapToDto(courseM);

        UriBuilder uriBuilder = uriInfo.getBaseUriBuilder();
        uriBuilder.path("maintenance/student/retrieveone/" + createdCourseDto.getCourseId());
        URI uri = uriBuilder.build();
        return Response.created(uri).build();
    }

    @PUT
    @Path("update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response update(CourseDto courseDto, @Context UriInfo uriInfo) throws Exception {
        UIResponse<CourseDto> response = new UIResponse<CourseDto>();
        CourseM courseM = courseRepo.findOne(courseDto.getCourseId());
        if (courseM != null) {
            courseMapper.mapToEntity(courseM, courseDto);
            courseM = courseRepo.save(courseM);
            CourseDto updatedCourseDto = courseMapper.mapToDto(courseM);
            response.success(updatedCourseDto, UPDATE_SUCCESS_MESSAGE);
            return Response.ok(response).build();
        } else {
            response.error(NOT_FOUND_ERROR_MESSAGE);
            return Response.status(Status.NOT_FOUND).entity(response).build();
        }
    }

    @DELETE
    @Path("delete/{courseid}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("courseid") long courseId) throws Exception {
        UIResponse<CourseDto> response = new UIResponse<CourseDto>();
        CourseM courseM = courseRepo.findOne(courseId);
        if (courseM != null) {
            CourseDto courseDto = courseMapper.mapToDto(courseM);
            courseRepo.delete(courseM);
            response.success(courseDto, DELETE_SUCCESS_MESSAGE);
            return Response.ok(response).build();
        } else {
            response.error(NOT_FOUND_ERROR_MESSAGE);
            return Response.status(Status.NOT_FOUND).entity(response).build();
        }
    }
}
