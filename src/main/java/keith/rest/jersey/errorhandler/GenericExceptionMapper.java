package keith.rest.jersey.errorhandler;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import keith.domain.dto.StudentDto;
import keith.domain.dto.UIResponse;

/**
 * 
 * @author Keith F. Jayme
 *
 *         This serves as default Exception handler for Rest Resources that do not handle Exceptions. Even though they return the same
 *         status (500 Internal Server Error) as w/ the Else part, this ExceptionMapper enables JSON type response rather than the default
 *         html. Ideally this ExceptionMapper will not be used as Rest Resources should handle Exceptions to return appropriate Response
 *         Status that conform to Rest standards.
 * 
 */
@Provider
public class GenericExceptionMapper implements ExceptionMapper<Throwable> {

    @Override
    public Response toResponse(Throwable exception) {
        int status;
        if (exception instanceof WebApplicationException) {
            status = ((WebApplicationException) exception).getResponse().getStatus();
        } else {
            status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
        }
        UIResponse<StudentDto> response = new UIResponse<StudentDto>();
        response.error(exception);
        // return Response.status(status).entity(exception.getMessage()).type(MediaType.APPLICATION_JSON).build();
        return Response.status(status).entity(response).type(MediaType.APPLICATION_JSON).build();
    }
}
